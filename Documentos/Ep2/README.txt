Aluno: João Pedro de Aquino Corrêa Martins
Matrícula: 16/0046602
Professor: Renato Coral

Orientação a Objetos - 1º semestre de 2017

Exercício de Programação 2 

Jogo "Space Invaders em Java"

Este exercício busca avaliar os seguintes conceitos:  


	Agregação e Composição
	UML
	Threads
	Swing e awt
	Arquitetura de Eventos
	Testes Unitários

Para a execução deste projeto, baixe-o diretamente do git atual e extraia na sua pasta de preferência. Vá até a pasta Ep2 e clique duas vezes no arquivo Ep2.jar. Caso não funcione, abra o projeto no Netbeans IDE (qualquer versão acima da 6.1) e clique no botão Executar. 
Caso haja alguma outra dúvida a cerca desse projeto, entre em contato comigo através do email jpmartins201@live.com .
