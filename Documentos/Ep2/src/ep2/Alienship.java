package ep2;

import java.awt.Image;
import java.awt.Rectangle;
import java.util.concurrent.ThreadLocalRandom;

import javax.swing.ImageIcon;

public class Alienship extends Sprite{
	
	private int countAlien;

    public int getCountAlien() {
        return countAlien;
    }

    public void setCountAlien(int countAlien) {
        this.countAlien += countAlien;
    }
	
	public Alienship(int x, int y){
            super(x,y);
		
	    loadImage("images/alien_HARD.png");
            
            visible = true;
            this.x = ThreadLocalRandom.current().nextInt(0, Game.getWidth() - getWidth());
	}
	public void moveAlienship(){
            y += 1; 
	}
        public void alienExplosion(){
            loadImage("images/explosion.png");
        }
}
