package ep2;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.BufferedWriter;
import java.io.File;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.Timer;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Clock;
import java.util.Scanner;
import javax.swing.JFrame;

public class Map extends JPanel implements ActionListener {

    /**
	 * 
	 */
	private static final long serialVersionUID = 5405473608473941014L;
	/**
	 * 
	 */
    private final int SPACESHIP_X = 220;
    private final int SPACESHIP_Y = 430;
    public int level = 900;
    public int maxScore = 10;
    private final Timer timer_map;
    private final Timer timer_aliens;
    private final Timer timer_bonus;
    private final Image background;
    private final Spaceship spaceship;
    private List<Laser> lasers = new ArrayList<>();
    private List<Alienship> alienships = new ArrayList<>();
    private List<Bonus> bonus = new ArrayList<>();
    private final Application app;
    
    
    public Map(Application app) {
        
        addKeyListener(new KeyListerner());
        
        setFocusable(true);
        setDoubleBuffered(true);
        ImageIcon image = new ImageIcon("images/space.jpg");
        
        this.background = image.getImage();

        spaceship = new Spaceship(SPACESHIP_X, SPACESHIP_Y);
        
        timer_map = new Timer(Game.getDelay(), this);
        timer_map.start();
        timer_aliens = new Timer(level, new initAliens());
        timer_aliens.start();
        timer_bonus = new Timer(1000, new initBonus());
        timer_bonus.start();
        this.app = app;
    }
    
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(this.background, 0, 0, null);

        if(spaceship.getLife() <=0){
            writePlayerRank();
            drawGameOver(g);
            app.gameOver();
        }
        else if(spaceship.getScore() >= maxScore){
            level -= 100;
            maxScore += 10;
        }else{    
            draw(g);
            
            Toolkit.getDefaultToolkit().sync();
        }
    }

    public void writePlayerRank(){
        
        File archive = new File("src/general_rank.txt");
        try{
            if(!archive.exists()){
                archive.createNewFile();
            }
            
            FileWriter fw = new FileWriter(archive, true);
            BufferedWriter bw = new BufferedWriter(fw);
            
            bw.write("Player: " +spaceship.getScore());
            bw.newLine();
            bw.close();
            fw.close();
            
        }catch(IOException ex){
            ex.printStackTrace();
        }
    }
    
    
    private void draw(Graphics g) {
               
        // Draw spaceship and alienships
        g.drawImage(spaceship.getImage(), spaceship.getX(), spaceship.getY(), this);
        for (int i = 0; i < alienships.size(); i++){
            Alienship in = alienships.get(i);
            g.drawImage(in.getImage(), in.getX(), in.getY(), this);
	}
        
        List<Laser> lasers = spaceship.getLasers();
        
        for(int i =0; i < lasers.size(); i++){
            Laser l = (Laser) lasers.get(i);
            g.drawImage(l.getImage(), l.getX(), l.getY(), this);
            
        }
        g.setColor(Color.WHITE);
        g.drawString("Score: " + spaceship.getScore(), 5, 15);
        g.drawString("Lifes: " + spaceship.getLife(), 400, 15);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
       
        
        updateSpaceship();
        
        List<Laser> lasers = spaceship.getLasers();
   
        for(int i = 0; i < lasers.size(); i++){
            
            Laser l = (Laser) lasers.get(i);
            if(l.isVisible()){
                l.bounds();
            }
            else{
                lasers.remove(i);
            }
            
        }
        
        for (int i = 0; i < alienships.size(); i++) {
            Alienship in = alienships.get(i);

            if (in.isVisible()) {
                in.moveAlienship();
            } else
                alienships.remove(i);			
        }
        
        for(int j =0; j<bonus.size(); j++){
            Bonus bon = bonus.get(j);
            
            if(bon.isVisible()){
                bon.moveBonus();
            } else
                bonus.remove(j);
        }
        checkCollision();
        
        repaint();
    }
    
    @SuppressWarnings("unused")
	private void drawMissionAccomplished(Graphics g) {

        String message = "MISSION ACCOMPLISHED";
        Font font = new Font("Helvetica", Font.BOLD, 14);
        FontMetrics metric = getFontMetrics(font);

        g.setColor(Color.white);
        g.setFont(font);
        g.drawString(message, (Game.getWidth() - metric.stringWidth(message)) / 2, Game.getHeight() / 2);
        spaceship.setVisible(false);
    }
    
    @SuppressWarnings("unused")
	private void drawGameOver(Graphics g) {

        String message = "Game Over";
        Font font = new Font("Helvetica", Font.BOLD, 50);
        FontMetrics metric = getFontMetrics(font);

        g.setColor(Color.white);
        g.setFont(font);
        g.drawString(message, (Game.getWidth() - metric.stringWidth(message)) / 2, Game.getHeight() / 2);
        
    }
    
    private void updateSpaceship() {
        spaceship.move();
    }
    public void checkCollision(){
        Rectangle spaceShape = spaceship.getBounds();
        Rectangle alienShape;
        Rectangle laserShape;
        Rectangle bonusShape;
               
        for(int i = 0; i < alienships.size(); i++){
            Alienship tempAlien = alienships.get(i);
            alienShape = tempAlien.getBounds();
            
            if(spaceShape.intersects(alienShape)){
                spaceship.setScore(1);
                tempAlien.setVisible(false);
                alienships.remove(i);
                spaceship.removeLife(1);
                if(spaceship.getLife() <= 0){
                    spaceship.setVisible(false);
                    alienships.removeAll(alienships);
                }
            }
        }
        for(int i = 0; i<bonus.size(); i++){
            Bonus tempBonus = bonus.get(i);
            bonusShape = tempBonus.getBounds();
            
            if(spaceShape.intersects(bonusShape)){
                spaceship.setScore(10);
                tempBonus.setVisible(false);
                bonus.remove(i);
            }
        }
        
        
        List<Laser> lasers = spaceship.getLasers();
        
        for(int i = 0; i < lasers.size(); i++){
            
            Laser tempLaser = lasers.get(i);
            laserShape = tempLaser.getBounds();
            
            for(int j = 0; j < alienships.size(); j++){
            
                Alienship tempAlien = alienships.get(j);
                alienShape = tempAlien.getBounds();
                
                if(laserShape.intersects(alienShape)){
                   // tempAlien.alienExplosion();
                    spaceship.setScore(1);
                    tempAlien.setVisible(false);
                    tempLaser.setVisible(false); 
                    lasers.remove(i);
                }
            }
        }
        
    }
    
    private class KeyListerner extends KeyAdapter {
        
        @Override
        public void keyPressed(KeyEvent e) {
            spaceship.keyPressed(e);
        }

        @Override
        public void keyReleased(KeyEvent e) {
            spaceship.keyReleased(e);
        }
    }
    
    private class initAliens implements ActionListener {
        public void actionPerformed(ActionEvent evt) {
            alienships.add(new Alienship(0, 0));
        }
    };
    private class initBonus implements ActionListener {
        public void actionPerformed(ActionEvent eae){
            bonus.add(new Bonus(0,0));
        }
    }
}
