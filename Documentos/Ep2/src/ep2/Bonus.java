
package ep2;

import java.awt.Image;
import java.awt.Rectangle;
import java.util.concurrent.ThreadLocalRandom;
import javax.swing.ImageIcon;


public class Bonus extends Sprite{
    private int countBonus;

    public int getCountBonus() {
        return countBonus;
    }
    
    
    public Bonus(int x, int y){
        super(x, y);
        loadImage("star.png");
        
        visible = true;
        this.x = ThreadLocalRandom.current().nextInt(0, Game.getWidth() - getWidth());
    }
    
    public void moveBonus(){
        y += 1;
    }
}
