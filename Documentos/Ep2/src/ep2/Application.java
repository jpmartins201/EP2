 package ep2;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;


public class Application extends JFrame {
    
        public FirstMenu firstScreen;
        public Map map;
        
	private static final long serialVersionUID = -4958031344659631860L;
        
        public void gameOver(){
            
            this.map.setVisible(false);
            this.firstScreen.setVisible(true);
        }

	public Application() {

        JMenuBar menuBar = new JMenuBar();
        
        JMenu menu = new JMenu("Menu");
        
        JMenuItem about = new JMenuItem("About ");
        
        JMenuItem exit = new JMenuItem("Exit");
               
        
        about.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent e){
                JOptionPane.showMessageDialog(null, "Developed by João Pedro", "Informations", JOptionPane.INFORMATION_MESSAGE);
            }
        });
        
        exit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                System.exit(0);
            }
        });
        
        menu.add(about);
        menu.add(exit);
        
        menuBar.add(menu);
        
        setJMenuBar(menuBar);
        firstScreen = new FirstMenu(this);
        add(firstScreen);    
      

        setSize(Game.getWidth(), Game.getHeight());

        setTitle("Space Combat Game");
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);        
    }
    
    public static void main(String[] args) {
        
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                Application app = new Application();
                app.setVisible(true);
            }
        });
    }
}
