package ep2;

import java.awt.Image;
import java.awt.Rectangle;

import javax.swing.ImageIcon;

public class Laser extends Sprite
{
    public Laser(int x, int y)
    {	
	super(x, y);
		
	loadImage("images/missile.png");
		
        visible = true;
    }
	public void bounds()
        {
            this.y -= 4;
            if(this.y > 500){ //Limite da tela
		visible = false;
		}
	}
}
